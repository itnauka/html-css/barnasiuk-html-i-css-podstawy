### Napisz stronę która będzie zawierać:


* oddzielny plik .css, ✅

* nazwę strony "Moja strona", ✅

* biały nagłówek "Moja strona", ✅

* zielone tło na całej stronie, ✅

* hiperłącza z kolorem "aqua" bez podkreśleń, które są w liście nieuporządkowanej (treść listy jest dowolna), ✅

* lista ma być bez kropek na początku, ✅

* dowolny tekst, ✅

* obrazek z szerokością 600px i wysokością 600px Link do obrazka, alt ma być z treścią "Paryż", ✅

* wszystko ma być w jednym tagu div i na środku strony ✅

### Pytania do tego zadania
1. Wyślij wynik