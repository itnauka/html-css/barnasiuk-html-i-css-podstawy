<div align="right">
    <img src="https://img.shields.io/badge/HTML5-E44D26?style=for-the-badge&logo=html5&logoColor=white" /> <br>
    <img src="https://img.shields.io/badge/CSS3-2965f1?style=for-the-badge&logo=css3&logoColor=white" />
</div>  

<div align="center">
<h1>Moja Podróź przez Kurs "HTML i CSS - Podstawy"</h1>
<a  href="https://www.udemy.com/course/html-i-css-podstawy/">
<img src="https://img-c.udemycdn.com/course/750x422/5563450_21de.jpg" width="500">
</a>
</div>  

## Wprowadzenie

Witam Chciałbym podzielić się moją przygodą z kursem "HTML i CSS - Podstawy", który niedawno ukończyłem. Ten kurs okazał się być kluczem do otwarcia dla mnie drzwi do fascynującego świata web development. Jako początkujący programista, zawsze interesowałem się tworzeniem stron internetowych i szukałem sposobu, aby zacząć od podstaw.

## Spis treści

<details >
    <summary><a href="01.Fromatowanie%20tekstu">01.Fromatowanie tekstu</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;h2&gt;&lt;/h2&gt;</dd>
        <dd>&lt;h3&gt;&lt;/h3&gt;</dd>
        <dd>&lt;h4&gt;&lt;/h4&gt;</dd>
        <dd>&lt;h5&gt;&lt;/h5&gt;</dd>
        <dd>&lt;h6&gt;&lt;/h6&gt;</dd>
        <dd>&lt;b&gt;&lt;/b&gt;</dd>
        <dd>&lt;strong&gt;&lt;/strong&gt;</dd>
        <dd>&lt;i&gt;&lt;/i&gt;</dd>
        <dd>&lt;sub&gt;&lt;/sub&gt;</dd>
        <dd>&lt;sup&gt;&lt;/sup&gt;</dd>
        <dd>&lt;u&gt;&lt;/u&gt;</dd>
        <dd>&lt;del&gt;&lt;/del&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="02.Style">02.Style</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;style&gt;&lt;/style&gt;</dd>
        <dt>CSS</dt>
        <dd>color</dd>
        <dd>background</dd>
        <dd>font-size</dd>
        <dd>font-weight</dd>
        <dd>font-style</dd>
    </dl>
</details>
<details>
    <summary><a href="03.Kontener%20div">03.Kontener div</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;div&gt;&lt;/div&gt;</dd>
        <dt>CSS</dt>
        <dd>background-color</dd>
        <dd>text-align</dd>
        <dd>color</dd>
    </dl>
</details>
<details>
    <summary><a href="04.Umieszczenie%20styli">04.Umieszczenie styli</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;style&gt;&lt;/style&gt;</dd>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;h2&gt;&lt;/h2&gt;</dd>
        <dd>&lt;h3&gt;&lt;/h3&gt;</dd>
        <dt>CSS</dt>
        <dd>color</dd>
</details>
<details>
    <summary><a href="05.Klasy%20oraz%20id">05.Klasy oraz id</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dt>CSS</dt>
        <dd>.classname</dd>
        <dd>#idname</dd>
        <dd>font-weight</dd>
        <dd>color</dd>
        <dd>font-size</dd>
        <dd>font-style</dd>
    </dl>
</details>
<details>
    <summary><a href="06.Formularz">06.Formularz</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;input&gt;</dd>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dd>&lt;label&gt;&lt;/label&gt;</dd>
        <dd>&lt;form&gt;&lt;/form&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="07.Listy">07.Listy</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;link&gt;</dd>
        <dd>&lt;ul&gt;&lt;/ul&gt;</dd>
        <dd>&lt;ol&gt;&lt;/ol&gt;</dd>
        <dd>&lt;li&gt;&lt;/li&gt;</dd>
        <dt>CSS</dt>
        <dd>list-style</dd>
        <dd>font-size</dd>
    </dl>
</details>
<details>
    <summary><a href="08.Tabele">08.Tabele</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;table&gt;&lt;/table&gt;</dd>
        <dd>&lt;tr&gt;&lt;/tr&gt;</dd>
        <dd>&lt;th&gt;&lt;/th&gt;</dd>
        <dd>&lt;td&gt;&lt;/td&gt;</dd>
        <dd>&lt;thead&gt;&lt;/thead&gt;</dd>
        <dd>&lt;tbody&gt;&lt;/tbody&gt;</dd>
        <dt>CSS</dt>
        <dd>margin</dd>
        <dd>padding</dd>
        <dd>border</dd>
    </dl>
</details>
<details>
    <summary><a href="09.Obrazy">09.Obrazy</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;img&gt;</dd>
        <dd>&lt;link&gt;</dd>
        <dt>CSS</dt>
        <dd>width</dd>
    </dl>
</details>
<details>
    <summary><a href="10.Hiper%C5%82%C4%85cza">10.Hiperłącza</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;a&gt;</dd>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dt>CSS</dt>
        <dd>margin</dd>
        <dd>padding</dd>
        <dd>box-sizing</dd>
        <dd>color</dd>
        <dd>font-size</dd>
        <dd>display</dd>
        <dd>text-decoration</dd>
        <dd>background-color</dd>
        <dd>font-size</dd>
        <dd>font-weight</dd>
        <dd>font-style</dd>
        <dd>border</dd>
        <dd>margin-bottom</dd>
    </dl>
</details>
<details>
    <summary><a href="11.Margin%20i%20padding">11.Margin i padding</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;div&gt;&lt;/div&gt;</dd>
        <dt>CSS</dt>
        <dd>margin</dd>
        <dd>padding</dd>
        <dd>width</dd>
        <dd>height</dd>
        <dd>background</dd>
        <dd>box-sizing</dd>
        <dd>padding-top</dd>
        <dd>padding-left</dd>
        <dd>padding-bottom</dd>
        <dd>padding-right</dd>
    </dl>
</details>
<details>
    <summary><a href="12.Kolory">12.Kolory</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;div&gt;&lt;/div&gt;</dd>
        <dt>CSS</dt>
        <dd>background-color</dd>
    </dl>
</details>
<details>
    <summary><a href="13.Tagi%20semantyczne">13.Tagi semantyczne</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;header&gt;&lt;/header&gt;</dd>
        <dd>&lt;nav&gt;&lt;/nav&gt;</dd>
        <dd>&lt;main&gt;&lt;/main&gt;</dd>
        <dd>&lt;article&gt;&lt;/article&gt;</dd>
        <dd>&lt;aside&gt;&lt;/aside&gt;</dd>
        <dd>&lt;footer&gt;&lt;/footer&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="Zadanie1">Zadanie1</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;div&gt;&lt;/div&gt;</dd>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;ul&gt;&lt;/ul&gt;</dd>
        <dd>&lt;hl&gt;&lt;/hl&gt;</dd>
        <dd>&lt;a&gt;&lt;/a&gt;</dd>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dd>&lt;img&gt;&lt;/img&gt;</dd>
        <dt>CSS</dt>
        <dd>font-family</dd>
        <dd>background-color</dd>
        <dd>border</dd>
        <dd>text-align</dd>
        <dd>margin-left</dd>
        <dd>margin-right</dd>
        <dd>list-style</dd>
        <dd>text-decoration</dd>
        <dd>color</dd>
        <dd>width</dd>
        <dd>height</dd>
    </dl>
</details>

## Co Zawiera Kurs?

Kurs "HTML i CSS - Podstawy" jest kompleksowym przewodnikiem dla osób, które chcą nauczyć się tworzyć własne strony internetowe od podstaw. Zawiera on:

- **Podstawy HTML**: Zrozumienie struktury dokumentu HTML, tworzenie nagłówków, paragrafów, list, linków i innych elementów tekstowych.
- **Podstawy CSS**: Opanowanie sztuki stylizacji stron internetowych za pomocą kaskadowych arkuszy stylów, kontrola kolorów, czcionek, marginesów i paddingu.
- **Formularze**: Nauka tworzenia formularzy.
- **Organizacja i Struktura Kodu**: Poznanie dobrych praktyk programistycznych, które są kluczem do czytelnego i efektywnego kodu.

## Czego Się Nauczyłem

Podczas kursu zdobyłem solidne podstawy w zakresie HTML i CSS, które są niezbędne do tworzenia prostych, ale funkcjonalnych stron internetowych. Oto kilka kluczowych umiejętności, które nabyłem:

- Jak pisać kod HTML i CSS, aby stworzyć atrakcyjne i funkcjonalne strony internetowe.
- Jak łączyć HTML i CSS, aby nadać swojej stronie unikalny wygląd.
- Jak tworzyć formularze i dodawać interakcje, takie jak przyciski.
- Jak organizować i strukturyzować kod, aby był czytelny i efektywny.

## Dlaczego Wybrałem Ten Kurs?

Wybór kursu "HTML i CSS - Podstawy" był dla mnie oczywisty ze względu na jego bezpośrednie podejście do nauki. Kurs jest przeznaczony dla nowicjuszy w programowaniu, studentów oraz osób chcących rozpocząć karierę w web development. Jego struktura i materiały są tak przygotowane, aby każdy mógł zrozumieć podstawy tworzenia stron internetowych.

## Podsumowanie

Ukończenie kursu "HTML i CSS - Podstawy" było dla mnie kamieniem milowym w mojej podróży do web development. Dzięki temu kursowi nie tylko zdobyłem podstawowe umiejętności w HTML i CSS, ale także zainspirowałem się do dalszej nauki i rozwoju w tej dziedzinie. Serdecznie zachęcam każdego, kto jest zainteresowany tworzeniem stron internetowych, do spróbowania sił w tym kursie. Naprawdę warto!

<div>
    <a href="mailto:jackoski@gmail.com" >
    <img src="https://img.shields.io/badge/Jacek-Podg%C3%B3rni-teal">
    </a>  
</div>

![HTML5](https://img.shields.io/badge/HTML%205-grey?style=for-the-badge&logo=html5)  
![CSS3](https://img.shields.io/badge/CSS%203-grey?style=for-the-badge&logo=css3&logoColor=blue)  
![GitLab Badge](https://img.shields.io/badge/GitLab-FC6D26?logo=gitlab&logoColor=fff&style=plastic)  

